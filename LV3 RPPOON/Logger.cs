﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    class Logger
    {
        private static Logger instance;
        string filePath;
        bool appendText;
        private Logger()
        {
            this.appendText = false;
            this.filePath = "log.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
                instance = new Logger();
            return instance;
        }

        public void Log(string message)
        {

            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, appendText))
            {
                writer.WriteLine(message);
                appendText = true;
            }
        }
        public void SetFilePath(string filePath)
        {
            this.filePath = filePath;
        }
    }
}

