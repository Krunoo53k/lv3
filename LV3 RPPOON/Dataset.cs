﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    class Dataset : Prototype
    {
        private List<List<string>> data;
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(Dataset dataset)
        {
            data = new List<List<string>>();
            foreach (var element in dataset.data)
            {
                data.Add(element);
            }
        }
        public Dataset(string filePath):this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public Dataset(List<List<string>> data)
        {
            this.data = data;
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
               
                    while((line=reader.ReadLine())!=null)
                    {
                        List<string> row = new List<string>();
                        string[] items = line.Split(',');
                        foreach(string item in items)
                        {
                            row.Add(item);
                        }
                        this.data.Add(row);
                    }

            }
        }
        public IList<List<string>>GetData()
        {
            return
                new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone() => new Dataset(this);
    }
}
