﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    public class Director
    {
        IBuilder builder;

        public Director(IBuilder builder) { this.builder = builder; }
        public void SetBuilder(IBuilder builder) { this.builder = builder; }

        public ConsoleNotification ErrorNotification(string author)
        {
            return  builder.SetAuthor(author).SetLevel((Category)0).SetText("This is an error message").Build();
        }
        public ConsoleNotification AlertNotification(string author)
        {
            return builder.SetAuthor(author).SetLevel((Category)1).SetText("This is an alert message").Build();
        }
        public ConsoleNotification InfoNotification(string author)
        {
            return builder.SetAuthor(author).SetLevel((Category)2).SetText("This is an informational message").Build();
        }
    }
}
