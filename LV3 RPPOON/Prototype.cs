﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    public interface Prototype
    {
        Prototype Clone();
    }
}
