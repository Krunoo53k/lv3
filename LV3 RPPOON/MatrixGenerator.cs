﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random generator;
        private MatrixGenerator()
        {
            this.generator = new Random();
        }
        public static MatrixGenerator GetInstance()
        {
            if(instance==null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }
        public double[][] GetMatrix(int sizeX, int sizeY)
        {
            double[][] matrix = new double[sizeX][];
            for (int i = 0; i < sizeX; i++)
            { 
                matrix[i] = new double[sizeY];
            
                for(int j=0;j<sizeY;j++)
                {
                    matrix[i][j] = generator.NextDouble();
                }
            }
            return matrix;
        }
    }
}
